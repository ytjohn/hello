package main
// not actually working

import (
	"net"
	"fmt"
	"log"
)

//var cidr string = "10.10.0.0/22"

func doit(cidr string) {

	ipv4Addr, ipv4Net, err := net.ParseCIDR(cidr)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(ipv4Addr)
	fmt.Println(ipv4Net)
}

func main() {

	doit("10.10.0.99/22")
	doit("2001:db8:a0b:12f0::1/32")
	doit("10.10.0.1")

	fmt.Println("see if an ip or cidr")
	//if net.IP("10.10.0.1")
}
