package main

import (
	"fmt"
)

func Sqrt(x float64) float64 {

	fmt.Printf("Find SqRt of %v, ", x)
	z := 1.0
	last := z
	attempts := 4
	fl_attempts := float64(attempts)


	// adjust attempt count
	for x/fl_attempts > fl_attempts {
		fl_attempts = fl_attempts * 10.0
		attempts = attempts + 5
	}

	fmt.Printf("will make %v attempts\n", attempts)
	for i := 1; i < attempts + 1; i++ {

		z -= (z*z - x) / (2 * z)
		truncated := float64(int(z*100)) / 100

		if last == truncated {

			fmt.Printf(" That took %v attempts\n", i)
			fmt.Printf(" %v^2 = %v\n", truncated, truncated*truncated)

			return truncated
		}
		last = truncated

	}
	fmt.Printf("I give up after %v attempts.\n", attempts)
	return z
}

func main() {
	fmt.Println(Sqrt(2))
	fmt.Println(Sqrt(18))
	fmt.Println(Sqrt(39239))
	fmt.Println(Sqrt(9939234))
	fmt.Println(Sqrt(3463636339234))
	fmt.Println(Sqrt(343433463636339234))

}
